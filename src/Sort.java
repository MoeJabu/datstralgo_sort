/**
 * Class that allows to sort a given Array of Students to be sorted by their ages as well as by their names.
 */
public class Sort {

    /**
     * Array to be sorted
     */
    private Student[] student_array;

    public Sort(Student[] student_array) {
        this.student_array = student_array;
    }

    public int getLengthOfArray() {
    	return this.student_array.length;
    }

    
    /**
     * Sorts the students array by age in ascending order
     */
    public void sortByAgeAscending(int i, int j){
    	if(i >= j) {
    		return;
    	}
    	
    	int p = Math.floorDiv((i+j), 2);
    	this.sortByAgeAscending(i, p);
    	this.sortByAgeAscending(p+1,j);
    	
    	if(this.student_array[j].getAge()<this.student_array[p].getAge()) {
    		swap(j, p);
    	}
    	this.sortByAgeAscending(i, j-1);
    	
    }
    
    /**
     * Sorts the students array by name in ascending order
     */
    public void sortByNameAscending(int i, int j){
    	
    	if(i >= j) {
    		return;
    	}
    	
    	int p = Math.floorDiv((i+j), 2);
    	this.sortByNameAscending(i, p);
    	this.sortByNameAscending(p+1,j);
    	
    	
    	
    	if(this.student_array[j].getName().compareTo(this.student_array[p].getName()) < 0) {
    		swap(j, p);
    	}
    	this.sortByNameAscending(i, j-1);

    }

    /**
     * Sorts the students array by age in descending order
     */
    public void sortByAgeDescending(int i, int j){
    	if(i >= j) {
    		return;
    	}
    	
    	int p = Math.floorDiv((i+j), 2);
    	this.sortByAgeDescending(i, p);
    	this.sortByAgeDescending(p+1,j);
    	
    	if(this.student_array[j].getAge()>this.student_array[p].getAge()) {
    		swap(j, p);
    	}
    	this.sortByAgeDescending(i, j-1);
    	
    }
    

    /**
     * Sorts the students array by name in descending order
     */
    public void sortByNameDescending(int i, int j){
    	if(i >= j) {
    		return;
    	}
    	
    	int p = Math.floorDiv((i+j), 2);
    	this.sortByNameDescending(i, p);
    	this.sortByNameDescending(p+1,j);
    	
    	
    	if(this.student_array[j].getName().compareTo(this.student_array[p].getName()) > 0) {
    		swap(j, p);
    	}
    	this.sortByNameDescending(i, j-1);

    }
    

    /**
     * Swaps two elements within the array based on the indices i and j
     * @param i first index
     * @param j second index
     */
    public void swap(int i, int j){
        Student tmp=student_array[j];
        student_array[j]=student_array[i];
        student_array[i]=tmp;
    }

    /**
     * Returns a String representation of the sorted Array
     * using only the Students' names
     * @return String representation of the array
     */
    public String getNamesAsString(){
        StringBuilder result= new StringBuilder();
        for(Student student:student_array){
            result.append(student.getName());
            result.append("\n");
        }
        return result.toString();
    }

    /**
     * Returns a String representation of the sorted Array
     * using only the Students' age
     * @return String representation of the array
     */
    public String getAgesAsString(){
        StringBuilder result= new StringBuilder();
        for(Student student:student_array){
            result.append(student.getAge());
            result.append("\n");
        }
        return result.toString();
    }

    /**
     * Sets the students array
     * @param students student array
     */
    public void setStudent_array(Student [] students){
        this.student_array=students;
    }

}
